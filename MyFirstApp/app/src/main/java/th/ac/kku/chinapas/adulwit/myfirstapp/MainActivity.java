package th.ac.kku.chinapas.adulwit.myfirstapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_NAME = "name";
    public static final String EXTRA_PHONE = "phone";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((Button) findViewById(R.id.btnSubmit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });

    }

    public void sendMessage() {
        Intent intent = new Intent(this, SecondActivity.class);
        String name = ((EditText) findViewById(R.id.etName)).getText().toString();
        String phone = ((EditText) findViewById(R.id.etPhone)).getText().toString();
        intent.putExtra(EXTRA_NAME, name);
        intent.putExtra(EXTRA_PHONE, phone);
        startActivity(intent);
    }
}
