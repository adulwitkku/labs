package th.ac.kku.chinapas.adulwit.myfirstapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.Locale;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Intent intent = getIntent();
        String name = intent.getStringExtra(MainActivity.EXTRA_NAME);
        String phone = intent.getStringExtra(MainActivity.EXTRA_PHONE);
        TextView textView = findViewById(R.id.tvResult);
        if (Locale.getDefault().getCountry().equals("TH"))
            textView.setText(name + " มีหมายเลขโทรศัพท์คือ " + phone);
        else
            textView.setText(name + " has phone number as " + phone);
    }
}
